import { OS_Data } from "../integration/User Agent Test";

export function convertCsvFileToArrayOfOS_Data(path: string): OS_Data[] {
  let dataArray: OS_Data[] = [];

  cy.readFile(path).then((data) => {
    // Make rows
    data = data.split("\n");

    // Make the OS_Data objects by string cuting
    for (let i = 1; i < data.length; i++) {
      const fullUserAgent = data[i];
      const OS = data[i].split(",")[2].split(" ")[0];
      const ip = data[i].split(",")[1].split(" ")[0];
      const userAgent =
        data[i].substr(data[i].indexOf("; ") + 2, 5) === "Linux"
          ? "Linux"
          : data[i].split("(")[1].split(" ")[0].split(";")[0];

      // Push the final to the array
      dataArray.push({
        fullUserAgent: fullUserAgent,
        OS: OS,
        userAgent: userAgent,
        ip: ip,
      });
    }
  });

  return dataArray;
}
