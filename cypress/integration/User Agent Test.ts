import { convertCsvFileToArrayOfOS_Data } from "../support/functions";

enum OS_List {
  Windows = "Windows",
  Linux = "Linux",
  Mac = "Macintosh",
}

export interface OS_Data {
  OS: string;
  fullUserAgent: string;
  userAgent: string;
  ip: string;
}

const pathTo_csv_File = "cypress/fixtures/data.csv";
let assetion: boolean;

describe("Read", () => {
  before('Reading "data.csv" file and "BaW_Lists" file', () => {
    // Get the CSV file as 'data' variable
    cy.wrap({ records: convertCsvFileToArrayOfOS_Data(pathTo_csv_File) }).as(
      "records"
    );

    // Get the black ant white lists as 'BaW_Lists' variable
    cy.fixture("BaW_Lists.json").as("BaW_Lists");
  });

  it("Test the records", () => {
    cy.get("@records").then((records) => {
      cy.get("@BaW_Lists").then((BaW_Lists) => {
        // Loop of every record
        for (let record of records["records"]) {
          // Check if IP was blacklisted
          if (BaW_Lists["blackList"].includes(record.ip)) {
            cy.log(`IS A FAKE!!\nIP address ${record.ip} was blacklisted.`);
          }
          // Check if IP was whitelisted
          else if (!BaW_Lists["whiteList"].includes(record.ip)) {
            // Check if the system is not same like the user agent
            assetion = record.userAgent === OS_List[record.OS];
            if (!assetion)
              cy.log(
                `IS A FAKE!!\nIP address ${record.ip} appears to be fake.\nThe OS that appeared is: ${record.OS}.\nThe OS in the userAgent is: ${record.userAgent}.`
              ).then(() => {
                expect(assetion, record.ip).to.be.true;
              });
          }
        }
      });
    });
  });
});
